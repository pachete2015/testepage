import { Component, OnInit, NgZone, NgModule } from '@angular/core';
import { BLE } from '@ionic-native/ble/ngx'
import { ToastController, NavController, IonicModule, ModalController } from '@ionic/angular'
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router'
import { DetailPage } from '../detail/detail.page';
import { ɵBROWSER_SANITIZATION_PROVIDERS } from '@angular/platform-browser';


//#define SERVICE_UUID            "59b752ae-a33f-41c4-b2d1-1a465debd4bd" // UART service UUID
//#define CHARACTERISTIC_UUID_RX  "59b752ae-a33f-41c4-b2d1-1a465debd4bd"
//#define MALHA_DATA_CHAR_UUID       "59b752ae-a33f-41c4-b2d1-1a465debd4bd" 


@NgModule({
  imports: [
    IonicModule,
  ],
})
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  [x: string]: any;
  devices: any[] = [];
  peripheral: any = {};
  MALHA_DATA_CHAR_UUID: any;
  statusMessage: string;
  SERVICE_UUID: any;
  CHARACTERISTIC_UUID_RX: any;
  destination: NavigationExtras;
  id: string = null;
  constructor(public NavCtrl: NavController,
    private ToastCtrl: ToastController,
    private ble: BLE,
    private zone: NgZone,
    private router: Router,
    private route: ActivatedRoute,
    private modalController: ModalController,
    private NgZone: NgZone) {
  }
  ngOnInit() {
  }
  ionViewDidEnter() {
    console.log('ionViewDidEnter');
    this.scan();
  }
  async presentModal() {
    const modal = await this.modalController.create({
      component: DetailPage
    });
    return await modal.present();
  }
  scan() {
    this.setStatus('Buscando dispositivos BLE');
    this.devices = [];  // clear list
    this.ble.scan([], 5).subscribe(
      device => this.onDeviceDiscovered(device),
      error => this.scanError(error)
    );
    setTimeout(this.setStatus.bind(this), 5000, 'Scan complete');
  }
  onDeviceDiscovered(device) {
    console.log('Discovered ' + JSON.stringify(device, null, 2));
    this.NgZone.run(() => {
      this.devices.push(device);
    });
  }
  // If location permission is denied, you'll end up here
  async scanError(error) {
    this.setStatus('Error ' + error);
    let toast = this.ToastCtrl.create({
      message: 'Erro ao escanear os dispositivos',
      position: 'bottom',
      duration: 5000
    });
    (await toast).present();
  }

  showAlert(title, message) {
    let toast = this.toastCtrl.create({
      title: title,
      subTitle: message,
      position: 'center',
      buttons: ['Ok']
    });
    toast.present();
  }

  async setStatus(message) {
    console.log(message);
    this.NgZone.run(async () => {
      this.statusMessage = message;
      let toast = this.ToastCtrl.create({
        message: 'Scan completo',
        position: 'bottom',
        duration: 5000
      });
      (await toast).present();
    });
  }
  deviceSelected(device) {
    alert(JSON.stringify(device) + ' selected');
  }
  BleConnect(device) {
    this.ble.connect(device.id).subscribe(
      peripheral => this.onConnected(peripheral),
      peripheral => this.showAlert('Disconnected', 'Error')
    );

  }
  BleDisconnect() {
    this.ble.disconnect(this.peripheral.id).then(
      () => alert('Desconectado ' + JSON.stringify(this.peripheral)),
      () => alert('Erro ao desconectar ' + JSON.stringify(this.peripheral)));
  }
  onConnected(peripheral) {
    alert('Connected to ' + (peripheral.name || peripheral.id));
    this.NgZone.run(() => {
      this.peripheral = peripheral;
    });
    this.ble.startNotification(this.peripheral.id, '59b752ae-a33f-41c4-b2d1-1a465debd4bd', '59b752ae-a33f-41c4-b2d1-1a465debd4bd').subscribe(
      data => this.onButtonStateChange(data)

    )

  }
  onButtonStateChange(buffer: ArrayBuffer) {
    var data = new Uint8Array(buffer);
    this.zone.run(() => {
      this.peripheral = data[0];
      console.log(this.peripheral);
      if (this.peripheral !== this.peripheral) {
        alert(' 123');
      }

    });
  }
  async onDeviceDisconnected(peripheral) {
    const toast = await this.toastCtrl.create({
      message: 'The peripheral unexpectedly disconnected',
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }


}
