import { Component, OnInit, NgModule } from '@angular/core';
import { BLE } from '@ionic-native/ble/ngx';
import { NgZone } from '@angular/core';
import { NavController, } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute } from '@angular/router';

import { IonicModule } from '@ionic/angular';


const SERVICE_UUID = "59b752ae-a33f-41c4-b2d1-1a465debd4bd";
const CHARACTERISTIC_UUID_RX = "59b752ae-a33f-41c4-b2d1-1a465debd4bd";


@NgModule({
  imports: [

    IonicModule,

  ],

})
@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  peripheral: any = {};
  statusMessage: string;
  navegation: NavigationExtras;
  router: any;
  id: string = null;

  constructor(
    public navCtrl: NavController,
    public navController: NavController,
    private ble: BLE,
    private route: ActivatedRoute,
    private toastCtrl: ToastController,
    private ngZone: NgZone
  ) {

    this.id = this.route.snapshot.params['id'];
    console.log(this.id);
    this.LoadData();
  }

  ngOnInit() {

  }
  // the connection to the peripheral was successful

  LoadData() {
    console.log("chegou");

    //let device

    //this.setStatus('Connecting to ' + device.name || device.id)

    this.ble.connect(this.id).subscribe(
      peripheral => this.onConnected(peripheral),
      () => this.showAlert('Disconnected')
    );
  }


  onConnected(peripheral) {

    this.peripheral = peripheral;
    this.setStatus('Connected to ' + (peripheral.name || peripheral.id));

    this.ble.startNotification(this.peripheral.id, SERVICE_UUID, CHARACTERISTIC_UUID_RX).subscribe(
      data => this.onButtonStateChange(data),
      () => this.showAlert('Unexpected Error')
    )

  }

  onButtonStateChange(buffer: ArrayBuffer) {
    var data = new Uint8Array(buffer);
    console.log(data[0]);

    this.ngZone.run(() => {
      this.peripheral = data[0];
    });

  }

  // Disconnect peripheral when leaving the page
  ionViewWillLeave() {
    console.log('ionViewWillLeave disconnecting Bluetooth');
    this.ble.disconnect(this.peripheral.id).then(
      () => console.log('Disconnected ' + JSON.stringify(this.peripheral)),
      () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral))
    )
  }

  async showAlert(title) {
    let alert = this.toastCtrl.create({
      header: title,

      buttons: ['OK']
    });
    (await alert).present();
  }

  goToPageHome() {
    this.router.navigateByUrl('/home')
  }
  setStatus(message) {
    console.log(message);
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }
  
  /**disconnectDevice(){
    this.ble.disconnect(this.peripheral.id).then(
      () => console.log('Desconectando ' + JSON.stringify(this.peripheral)),
    this.router.navigateByUrl('/home')
    )
  }*/
}

